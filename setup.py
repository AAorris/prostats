"""prostats uses pstats to collect process statistics for elasticsearch."""

from setuptools import setup, find_packages

long_description = __doc__

setup(
    name='prostats',
    version='0.0.1',
    author='Aaron Morris',
    author_email='AAorris@github.com',
    description="Process stats oriented towards elasticsearch.",
    license='MIT',
    keywords='pstats',
    url='https://gitlab.com/AAorris/prostats.git',
    package_dir={'': '.'},
    packages=find_packages(),
    long_description=__doc__,
    classifiers=[
        'Development Status :: 1 - Planning',
])
