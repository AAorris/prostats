# prostats

Rate controlled process tracking sidecar that can upload to elastic search.

## Usage

```bash
▲ prostats python -m prostats.tracker -pid 2364 -r 100 -f process_cpu
{'_id': 'MShSxknA', '_type': 'process', 'process_cpu': 0.0, '_index': 'testing'}
{'_id': '9sHZqy7w', '_type': 'process', 'process_cpu': 0.7, '_index': 'testing'}
{'_id': 'maUWuU5g', '_type': 'process', 'process_cpu': 0.3, '_index': 'testing'}
{'_id': 'n6RPr9fg', '_type': 'process', 'process_cpu': 0.4, '_index': 'testing'}
{'_id': 'maUWuU5g', '_type': 'process', 'process_cpu': 0.3, '_index': 'testing'}
{'_id': 'wny+wLhw', '_type': 'process', 'process_cpu': 0.5, '_index': 'testing'}
{'_id': '9sHZqy7w', '_type': 'process', 'process_cpu': 0.7, '_index': 'testing'}
{'_id': 'maUWuU5g', '_type': 'process', 'process_cpu': 0.3, '_index': 'testing'}
{'_id': 'n6RPr9fg', '_type': 'process', 'process_cpu': 0.4, '_index': 'testing'}
^C
Recorded 9 stats.
```

CI with gitlab runners and docker

```
sudo docker run -d\
 --name gitlab-runner\
 --restart always\
 -v /srv/gitlab-runner/config:/etc/gitlab-runner\
 gitlab/gitlab-runner:latest
```
