"""Process tracking for elastic."""

import datetime
import fnmatch
import json
import os
import platform
import time
import traceback
import uuid
from md5 import md5
from base64 import b64encode
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
from psutil import Process, NoSuchProcess, swap_memory, cpu_percent



statistic = {
    # Functions for calculating a metric value.
    # A process handle is given to be used (or not)
    'timestamp': lambda ps: datetime.datetime.utcnow().isoformat(),
    'machine_mac': lambda ps: uuid.getnode(),
    'machine_name': lambda ps: platform.node(),
    'machine_swap': lambda ps: swap_memory().percent,
    'machine_cpu': lambda ps: cpu_percent(),
    'process_name': lambda ps: ps.name(),
    'process_user': lambda ps: ps.username(),
    'process_status': lambda ps: ps.status(),
    'process_mem': lambda ps: round(ps.memory_percent(), 2),
    'process_cpu': lambda ps: ps.cpu_percent(),
    'process_fds': lambda ps: ps.num_fds(),
    'process_threads': lambda ps: ps.num_threads(),
    'process_cmdline': lambda ps: filter(None, ps.cmdline()),
    'process_nice': lambda ps: ps.nice(),
    'cpu_sys': lambda ps: ps.cpu_times().system,
    'cpu_user': lambda ps: ps.cpu_times().user,
    'cpu_syschild': lambda ps: ps.cpu_times().children_system,
    'cpu_userchild': lambda ps: ps.cpu_times().children_user,
    'memory_rss': lambda ps: ps.memory_full_info().rss,
    'memory_uss': lambda ps: ps.memory_full_info().uss,
    'memory_vms': lambda ps: ps.memory_full_info().vms,
    'switches_voluntary': lambda ps: ps.num_ctx_switches().voluntary,
    'switches_involuntary': lambda ps: ps.num_ctx_switches().involuntary,
}


def track(pid, rate, keys=None):
    global statistic
    ps = Process(pid or os.getpid())
    try:
        while ps.is_running:
            if not keys:
                keys = statistic.keys()
            result = {key: value(ps) for key, value in statistic.items()
                      if key in keys}
            result['_index'] = "testing"
            result['_type'] = "process"
            result['_id'] = b64encode(md5(str(result)).digest())[::3]
            yield result
            time.sleep(60.0 / rate)
    except NoSuchProcess:
        return


def _flatten(prefix, data):
    data = data._asdict()
    return {'{}.{}'.format(prefix, key): value for key, value in data.items()}


def main(argv=[]):
    import argparse
    parser = argparse.ArgumentParser(
        prog="nanofold.tracker",
        usage="Process tracking for nanofold.",
        description="Collects process stats on the given PID over time.")
    arg = parser.add_argument
    arg('--sample', action='store_true', help="Print sample collection")
    arg('-pid', type=int, help="process id to track")
    arg('-r', '--rate', type=float, default=60, help="refresh rate (per minute)")
    arg('-f', '--fields', help="comma separated fields. (globs supported)")
    arg('-es', '--elasticsearch', help="elastic url")
    args = parser.parse_args(argv)

    stat_keys = statistic.keys()
    if args.fields:
        sources = set(args.fields.split(","))
        keys = set(statistic.keys())
        found = set()
        for pat in sources:
            found.update(fnmatch.filter(keys, pat))
        stat_keys = found

    if args.sample:
        print("\n".join(sorted(stat_keys)))
        return

    if args.elasticsearch:
        def log_stats(stats):
            try:
                es = Elasticsearch(args.elasticsearch)
                bulk(es, stats)
            except Exception as excp:
                print(traceback.format_exc())
    else:
        def log_stats(stats):
            print("Recorded {} stats.".format(len(stats)))

    # start tracking
    try:
        stats = []
        lasthash = ""  # Will only update if changes occured.
        for stat in track(args.pid, args.rate, stat_keys):
            hashed = stat.get("_id", "")
            if hashed != lasthash:
                print(stat)
                lasthash = hashed
            stats.append(stat)
    except KeyboardInterrupt:
        print("")  # there's a line of "^C from ctrl+c"
    finally:
        log_stats(stats)


if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
