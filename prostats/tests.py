import os
import pprint
import unittest

import psutil

from prostats import tracker


class TrackerTests(unittest.TestCase):
    def test_extract_stats(self):
        """Test that a dictionary of stats can be extracted from a process."""
        stats = tracker.track(os.getpid(), rate=1000)  # 1000 stats per minute
        for stat in range(10):
            stat = next(stats)
            pprint.pprint(stat)
            self.assertEqual(type(stat), dict)
